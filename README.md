# CSE356 Project 1 #

## Setting up the dev environment ##

Install [Vagrant 1.9.0](https://releases.hashicorp.com/vagrant/1.9.0/) as NFS file syncing does not work on Vagrant 1.9.1.  
For OS X users with Homebrew installed, type "brew cask install https://raw.githubusercontent.com/caskroom/homebrew-cask/b99d5280a3fae60bc18bf16ab314d9cc84e7af87/Casks/vagrant.rb"  
Type "vagrant up".  
To step inside the guest OS, type "vagrant ssh".  
