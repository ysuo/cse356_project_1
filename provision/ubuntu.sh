#!/usr/bin/env bash

apt-get update
apt-get install -y nginx
apt-get install -y python3-pip

apt-get install -y epel-release
apt-get install -y nginx
apt-get install -y apt-get-utils
apt-get install -y kernel-devel
apt-get install -y dkms
apt-get -y groups install "GNOME Desktop"
systemctl set-default graphical.target
apt-get -y install https://centos7.iuscommunity.org/ius-release.rpm
apt-get -y install python35u-pip
apt-get -y install nfs-utils nfs-utils-lib
apt-get -y install npm
npm -g install nodemon
npm -g install grunt-cli
npm -g install gulp-cli
npm -g install express-generator
apt-get -y install nginx
setsebool -P httpd_can_network_connect on
cp ./eliza/provision/nginx.conf /etc/nginx/
systemctl restart nginx
systemctl enable nginx
