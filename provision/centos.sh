#!/usr/bin/env bash

yum install -y epel-release
yum install -y nginx
yum install -y yum-utils
yum install -y kernel-devel
yum install -y dkms
yum -y groups install "GNOME Desktop"
systemctl set-default graphical.target
yum -y install https://centos7.iuscommunity.org/ius-release.rpm
yum -y install python35u-pip
yum -y install nfs-utils nfs-utils-lib
yum -y install npm
npm -g install nodemon
npm -g install grunt-cli
npm -g install gulp-cli
npm -g install express-generator
yum -y install nginx
setsebool -P httpd_can_network_connect on
cp ./eliza/provision/nginx.conf /etc/nginx/
systemctl restart nginx
systemctl enable nginx
