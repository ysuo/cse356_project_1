// require express
var express = require('express');
var path    = require('path');
var mongoose = require('mongoose');
var nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');
var reCAPTCHA = require('recaptcha2');
const SITE_KEY = '6Leu9hYUAAAAABTcBrcMg162BOzP2qBGyUWLOPOw';
const SECRET_KEY = '6Leu9hYUAAAAAPHWtTRkikSyRv2m5yh4TsqpBapO';

var verifyRecaptcha = new reCAPTCHA({
	siteKey: SITE_KEY,
	secretKey: SECRET_KEY
});
// create our router object
var router = express.Router();

// export our router
module.exports = router;

var host = null;

var transporter = nodemailer.createTransport(smtpTransport({
  service: 'Gmail',
  auth: {
    user: 'gofaraway456@gmail.com', // Your email id
    pass: '//account' // Your password
  }
}));

// route for our homepage
router.get('/', function(req, res) {
  // render layout.ejs with index.ejs as `body`.
  res.render('pages/adduser.ejs');
});

router.post('/adduser', function(req, res) {
	verifyRecaptcha.validateRequest(req)
		.then(function() {
			var username = req.body.username;
			var useremail = req.body.useremail;
			var userpass = req.body.userpass;
			host=req.get('host');

			var User = mongoose.model('User');
			var user_data = { "username": username, "useremail": useremail, "userpass": userpass, "status":false};
			var user = new User(user_data);
			user.save(function (err, user) {
				if (err) {
					console.log(err);
				} else {
					var id = user.id;
					var mailOptions={
						to : useremail,
						subject : 'Please activate your account',
						html : '<a href="http://'+req.get('host')+'/verify?id='+id+'">Activate</a>'
					}
					console.log(mailOptions);          
					transporter.sendMail(mailOptions, function(error, response){
						if(error){
							console.log(error);
							res.end("error");
						}else{
							res.redirect('/login');
						}
					});
				}
				res.end("Success!");
				// TODO: do registration using params in req.body
			});
		})
		.catch(function(errorCodes){      
			res.end("Captcha failed, sorry.");
			// TODO: take them back to the previous page
			// and for the love of everyone, restore their inputs
		});
	});

router.get('/verify',function(req,res){
	console.log(req.protocol+":/"+req.get('host'));
	if((req.protocol+"://"+req.get('host'))==("http://"+host)) {
    console.log("Domain is matched. Information is from Authentic email");
    var User = mongoose.model('User');
    var currentUser_id = req.query.id;
    User.findById(currentUser_id, function (err, found) {
      console.log(found);
      if(found.status == false) {
        found.status = true;
        found.save();
        console.log(found);
      }
      res.redirect('/login');
    });
  }
  else {
    res.end("<h1>Request is from unknown source");
  }
});

router.post('/login', function(req, res) {
  var username = req.body.username;
  var userpass = req.body.userpass;
  var User = mongoose.model('User');
  User.findOne({ username: username}, function(err, found) {
    if (err) {console.log(err);}

    if (found) {
      if(found.status == false) {
        console.log('no verify');
      }        
    } else {
        console.log("no sign up yet...");
    }
  });
  res.render('pages/login.ejs');
});

router.get('/login', function(req, res) {
  res.render('pages/login.ejs');
});

router.get('/logout', function(req, res) {

  res.render('pages/adduser.ejs');
});
