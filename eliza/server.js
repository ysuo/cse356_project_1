
// require our dependencies
var express        = require('express');
var expressLayouts = require('express-ejs-layouts');
var bodyParser     = require('body-parser');
var cookieParser   = require('cookie-parser')
var app            = express();
var mongoose       = require('mongoose');
var port           = process.env.PORT || 3000;

// use ejs and express layouts
app.set('view engine', 'ejs');
app.use(expressLayouts);

// use body parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(cookieParser());

// route our app
var router = require('./app/routes');
app.use('/', router);

// database
mongoose.connect('mongodb://localhost/test');
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  // we're connected!
});
var Schema = mongoose.Schema;
var user_schema = new Schema({ 
	username: {type: String, index: {unique: true, dropDups: true}}, 
	password: {type:String}, 
	email: {type: String, index: {unique: true, dropDups: true}},
	status: {type: Boolean}
});
var User = mongoose.model('User', user_schema);


// set static files (css and images, etc) location
app.use(express.static(__dirname + '/public'));

// start the server
app.listen(port, function() {
  console.log('app started');
});

